from requests.auth import HTTPBasicAuth
import logging
import os
import requests
#import urllib3
#urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class ALMException(Exception):
    pass


logger = logging.getLogger(__name__)


def logging_cfg(filename='hpalm.log'):
    """ Create a FileHandler based logfile for logging """
    global logger

    file_path = os.path.join(os.getcwd(), filename)

    logging.basicConfig(datefmt='%H:%M:%S',
                        format="[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s",
                        filename=file_path,
                        level=logging.NOTSET)

    logger = logging.getLogger(__name__)


class HPALM(object):
    headers = None

    def __init__(self, **kwargs):
        self.base_url = kwargs.get('base_url', None)
        self.username = kwargs.get('username', None)
        self.password = kwargs.get('password', None)
        self.domain = kwargs.get('domain', None)
        self.project = kwargs.get('project', None)
        self.verify = kwargs.get('verify', False)
        self.alm_version = kwargs.get('alm_version', 11)
        log_msg = kwargs.get('log_msg', 1)

        if not (kwargs['base_url'] or kwargs['username'] or kwargs['password'] or kwargs['domain'] or kwargs['project']):
            raise ALMException("Please provide all mandatory params")

        if log_msg:
            logging_cfg()

    def getheaders(self):
        return self.headers

    def is_authenticated(self):
        url = "{base_url}/qcbin/rest/is-authenticated".format(base_url=self.base_url)
        resp = requests.get(url, headers=self.getheaders())
        logger.debug(resp.text)
        return resp.status_code

    def login(self):
        """
        Login into hp alm
        """
        headers = {'Content-Type' : 'application/xml'}
        '''authurl = self.base_url + '/qcbin/rest/is-authenticated'
        resp = requests.get(authurl, headers=headers, verify=False)
        logger.debug("Response headers: %s" %resp.headers)
        lwssocookie = resp.headers['WWW-Authenticate']'''
        #headers = {'Cookie' : lwssocookie}
        headers["Accept"] = 'application/xml'
        resp = ''
        login_url = self.base_url + '/qcbin/authentication-point/authenticate'
        if self.alm_version == 11:
            resp = requests.get(login_url, headers=headers, auth=HTTPBasicAuth(self.username, self.password), verify=self.verify)
        #logger.debug("Login resposne headers %s" % resp.headers)
        qc_session = resp.headers['Set-Cookie']
        logger.debug("Is QC session launched: %s" % qc_session)
        #cookie = ";".join((lwssocookie, qc_session))

        self.headers = {'content-type' : 'application/xml'}
        self.headers['accept'] = 'application/xml'
        #self.headers['cookie'] = cookie
        #logger.info("Headers: %s" % self.headers)
        if resp.status_code == 200:
            print("%s  %s logged into alm" % (resp.status_code, self.username))
        else:
            raise ALMException("%s  %s failed to logging into HP ALM" % (resp.status_code, self.username))

        session_url = self.base_url + '/qcbin/rest/site-session'
        cookies = {'QCSession': qc_session}
        resp = requests.post(session_url, headers=self.headers, auth=HTTPBasicAuth(self.username, self.password))
        logger.debug("Site-session resposne headers %s" % resp.headers)
        logger.info("rest/site-session response code: %s  " % resp.status_code)
        return resp.status_code

    def logout(self):
        uri = self.base_url + '/qcbin/authentication-point/logout'
        resp = requests.get(uri, headers=self.headers)
        if resp.status_code == 200:
            logger.info("%s  %s logged out alm" % (resp.status_code, self.username))
        else:
            raise ALMException("%s  %s failed to loggedout from HP ALM" %(resp.status_code, self.username))
        return resp.status_code

    def getentity(self, entitytype, entityid=None, query=None):

        """Get an entity

        :param entityType: type of entity to get:

            tests/test-sets/test-configs/test-set-folders/test-instances/runs/release-cycles/defects

        :type entityType: str

        :param entityId: id of entity to get. If None returns all instances of entity

        :type entityId: str | int

        :param query: query string to filter data. e.g: {name[Basic]}

        :type query: str

        :return: requested entity(s)

        :rtype: dict

        """
